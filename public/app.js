angular
  .module('SingleLineSearchApp', ['ngMaterial'])
  .controller('addressCtrl', addressCtrl);

function addressCtrl($timeout, $q, $log, $http) {
  var ctrl = this;

  ctrl.querySearch = querySearch;
  ctrl.selectedItemChange = selectedItemChange;
  ctrl.candidates;
  ctrl.header;
  ctrl.info = '';

  var host = 'http://localhost:8000';

  function querySearch(query) {
    ctrl.candidates = [];
    var results = $http.get(host + '/getByName/' + query)
      .then(function (searchCandidates) {
        var pointAddresses = searchCandidates.data.pointAddresses;
        var farms = searchCandidates.data.farms;
        var pointOfInterests = searchCandidates.data.pointOfInterests;
        var adminAreas = searchCandidates.data.adminAreas;
        var roads = searchCandidates.data.roads;

        if (pointAddresses.length == 0 && farms.length == 0 && pointOfInterests.length == 0 && adminAreas.length == 0 && roads.length == 0) {
          ctrl.candidates = [];
        }
        else {
          if (pointAddresses.length != 0) {
            for (var i = 0; i < pointAddresses.length; i++) {
              var address = pointAddresses[i].address;
              ctrl.candidates.push({
                display: address.roadNumber + ' ' +
                  address.roadName + ' ' +
                  address.townName + ' ' +
                  address.provinceName,
                value: address.id,
                type: "point address"
              })
            }
          }
          if (farms.length != 0) {
            for (var i = 0; i < farms.length; i++) {
              var address = farms[i].address;
              ctrl.candidates.push({
                display: address.name + ' ' +
                  address.townName + ' ' +
                  address.provinceName,
                value: address.id,
                type: "farm"
              })
            }
          }
          if (pointOfInterests.length != 0) {
            for (var i = 0; i < pointOfInterests.length; i++) {
              var address = pointOfInterests[i].address;
              ctrl.candidates.push({
                display: address.name + ' ' + address.suburbName + ' ' +
                  address.townName + ' ' +
                  address.provinceName,
                value: address.id,
                type: "point of interest"
              })
            }
          }
          if (roads.length != 0) {
            for (var i = 0; i < roads.length; i++) {
              var address = roads[i].address;
              ctrl.candidates.push({
                display: address.name + ' ' +
                  address.townName + ' ' +
                  address.provinceName,
                value: address.id,
                type: "road"
              })
            }
          }
          if (adminAreas.length != 0) {
            for (var i = 0; i < adminAreas.length; i++) {
              var address = adminAreas[i].address;
              ctrl.candidates.push({
                display: address.suburbName + ' ' +
                  address.townName + ' ' +
                  address.provinceName,
                value: address.id,
                type: address.areaType.toLowerCase(),
                areaType: address.areaType
              })
            }
          }
        }
        return ctrl.candidates;
      });
    return results;
  }

  function selectedItemChange(item) {
    var data = '';
    ctrl.header = '';
    ctrl.info = '';
    if (item != undefined) {
      if (item.type == 'point address') {
        data = 'addresses';
        ctrl.header = 'Point Address data:';
      } else
        if (item.type == 'farm') {
          data = 'farms';
          ctrl.header = 'Farm data:';
        } else
          if (item.type == 'point of interest') {
            data = 'point-of-interests';
            ctrl.header = 'Point Of Interest data:';
          } else
            if (item.type == 'road') {
              data = 'roads';
              ctrl.header = 'Road data:';
            } else
              if (item.type == 'suburb' || item.type == 'town') {
                if (item.areaType == 'SUBURB') {
                  data = 'suburbs';
                } else if (item.areaType == 'TOWN') {
                  data = 'towns';
                }
                ctrl.header = data.charAt(0).toUpperCase() + data.substr(1, data.length - 2) + ' data:';
              }
      var results = $http.get(host + '/getById/' + data + '/' + item.value)
        .then(function (info) {
          ctrl.info = JSON.stringify(info.data, null, "\t");
          return ctrl.info;
        });
    }



    return results;
  }
}